const { app, BrowserWindow, protocol } = require("electron");
const path = require("path");
const url = require("url");

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require("electron-squirrel-startup")) {
    // eslint-disable-line global-require
    app.quit();
}

const createWindow = () => {
    // Create the browser window.

    const mainWindow = new BrowserWindow({ width: 1920, height: 1080 });

    mainWindow.loadURL(
        url.format({
            pathname:
                "index.html" /* Attention here: origin is path.join(__dirname, 'index.html') */,
            protocol: "file",
            slashes: true,
        })
    );

    mainWindow.on("closed", () => {
        // mainWindow = null;
        app.quit();
    });

    // mainWindow.webContents.openDevTools();
    // mainWindow.setFullScreen(true);
    mainWindow.setFullScreen(false);
    mainWindow.setMenu(null);
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
// app.on("ready", createWindow);

let lang = "en";
app.on("ready", () => {
    protocol.interceptFileProtocol(
        "file",
        (request, callback) => {
            const url =
                request.url.substr(7); /* all urls start with 'file://' */
            let callbackPath = {};
            if (lang === "es") {
                if (
                    url.includes("/espanol/") === false &&
                    url !== "/index.html"
                ) {
                    callbackPath = path.normalize(
                        `${__dirname}/electron/langs/${lang}/${url}`
                    );
                } else {
                    callbackPath = path.normalize(
                        `${__dirname}/electron/langs/${lang}/${url.replace(
                            "/espanol/",
                            ""
                        )}`
                    );
                }
            } else if (lang === "it") {
                if (
                    url.includes("/italian/") === false &&
                    url !== "/index.html"
                ) {
                    callbackPath = path.normalize(
                        `${__dirname}/electron/langs/${lang}/${url}`
                    );
                } else {
                    callbackPath = path.normalize(
                        `${__dirname}/electron/langs/${lang}/${url.replace(
                            "/italian/",
                            ""
                        )}`
                    );
                }
            } else {
                callbackPath = path.normalize(
                    `${__dirname}/electron/langs/${lang}/${url}`
                );
            }

            console.log(callbackPath);
            callback({ path: callbackPath });
        },
        (err) => {
            if (err) console.error("Failed to register protocol");
        }
    );
    createWindow();
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});

app.on("activate", () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow();
    }
});

app.on("web-contents-created", (e, contents) => {
    contents.on("new-window", (e, url) => {
        e.preventDefault();
        require("open")(url);
    });
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
